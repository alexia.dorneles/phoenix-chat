#PHOENIX CHATBOT
![Phoenix](https://scontent.fpoa1-1.fna.fbcdn.net/v/t1.0-9/46451072_343868002860277_6584010806923362304_n.jpg?_nc_cat=110&_nc_oc=AQmhIg0upc3cPZb_i6BIlRnbu2nnDhVbjjTtroKcpk55V2d2PejHSXsRucU1WC1UxkgBnGMsBRN8HkPvAVIDTBwY&_nc_ht=scontent.fpoa1-1.fna&oh=16cd729bde13fd71b08306ecca4cbf8f&oe=5DA51D1A)

### Required:
Node version 12.6.0 or higher.
ngrok account and path setted.

### Running
To run the project please follow the steps bellow:

1. `npm install`
2. `./ngrok http 5000` (in user directory)
3. `npm start`
4. Go to facebook configuration app at https://developers.facebook.com/apps/2257210394498418/webhooks/
  4.1. Find Phoenix app.
  4.2. Go to Webhook section.
  4.3. Edit subscription.
  4.4. Add the new url from https ngrok console.
  4.5. Check the `VERIFY_TOKEN` in .env file.

  ### Folder structure
  - **webhooks**: One time added code to integrate bot to facebook and dialogflow. Far from being object oriented but also
  not going to be changed very much.
  - **types**: Definition of Enums, Classes and Interfaces.
