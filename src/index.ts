import dotenv from 'dotenv'
import express from 'express'
import bodyParser from 'body-parser'

dotenv.config()

import { verifier, message } from './webhooks'

const { PORT } = process.env
const app = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.listen(PORT, () => console.log(`Express server is listening on port ${PORT}`))
app.get('/', verifier)
app.post('/', message)
