import { httpService } from '@services'
import { Usuario } from '../types/Usuario'
import { LoginResponse } from '@types'

class UserService {
  constructor() { }
  async userRegisterOrLogin(usuario: Usuario): Promise<LoginResponse> {
    return await httpService.post('/public/login/pacient', usuario)
  }
}

const userService = new UserService()
export { userService }
