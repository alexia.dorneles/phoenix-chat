import { URLs, HttpMethods } from '@types'
import { httpService } from '@services'

abstract class DefaultRequestBody {
  messaging_type: string
  abstract recipient: object
  message: object | undefined
  sender_action: string | undefined
}

class RequestBody extends DefaultRequestBody {
  constructor(public recipient: object, public message: object) {
    super()
  }
}

class FacebookService {

  private defaultRequestBody = { messaging_type: 'RESPONSE' } as DefaultRequestBody

  async sendMessage(messageText: string, userId: number): Promise<void> {
    const requestBody: RequestBody = { ...this.defaultRequestBody, message: { text: messageText } }
    await this.sendToFacebookMessenger(requestBody, userId)
  }

  async sendMessageBubble(userId: number): Promise<void> {
    const requestBody: RequestBody = { ...this.defaultRequestBody, sender_action: 'typing_on' }
    await this.sendToFacebookMessenger(requestBody, userId)
  }

  private async sendToFacebookMessenger(requestBody: RequestBody, userId: number): Promise<void> {
    requestBody.recipient = { id: userId }
    httpService.accessExternal(HttpMethods.POST, URLs.FACEBOOK_MESSAGE, requestBody)
  }
}

const facebookService = new FacebookService()
export { facebookService }
