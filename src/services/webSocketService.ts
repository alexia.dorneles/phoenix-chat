import SockJS from 'sockjs-client'
import Stomp from 'stompjs/lib/stomp'
import { URLs } from '@types'

class WebSocketService {
  private wsClient: any

  connect(connectCallback: Function, errorCalback: Function): void {
    this.resetWebSocketClient()
    const socket = new SockJS(URLs.WEBSOCKET_URL)
    const client = Stomp.Stomp.over(socket)
    this.executeConnection(client, connectCallback, errorCalback)
  }

  subscribeLinkQueue(userId: number, callback: Function): void {
    const url = `${URLs.LINK_QUEUE}${userId}`
    this.subscribe(url, callback)
  }

  subscribe(url: string, callback: Function): void {
    this.wsClient.subscribe(url, (data: { body: string }) => {
      const response = JSON.parse(data.body)
      callback(response)
    })
  }

  private resetWebSocketClient(): void {
    if (this.wsClient) this.wsClient = undefined
  }

  private executeConnection(client: any, connectCallback: Function, errorCalback: Function): void {
    client.debug = null
    client.connect({}, connectCallback, errorCalback)
    this.wsClient = client
  }
}

const webSocketService = new WebSocketService()
export { webSocketService }
