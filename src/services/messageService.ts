import { MessageResponse, MessageModel } from '@types'
import { httpService } from '@services'

class MessageService {
  async save(message: MessageModel): Promise<MessageResponse> {
    return await httpService.post('/mensagem', message)
  }
}

const messageService = new MessageService()
export { messageService }
