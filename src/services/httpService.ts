import axios from 'axios'
import { URLs, HttpMethods } from '@types'

const CONTENT_TYPE = { 'Content-Type': 'application/json' }
const _config = {
  headers: {
    ...CONTENT_TYPE,
  },
}

class HttpService {
  constructor() { }

  async get(url: string): Promise<any> {
    const response = await axios.get(`${URLs.BASE_URL}/${url}`, _config)
    return response.data
  }

  async post(url: string, data = {}): Promise<any> {
    const response = await axios.post(`${URLs.BASE_URL}/${url}`, data, _config)
    return response.data
  }

  async put(url: string, data = {}): Promise<any> {
    const response = await axios.put(`${URLs.BASE_URL}/${url}`, data, _config)
    return response.data
  }

  async delete(url: string): Promise<any> {
    const response = await axios.delete(`${URLs.BASE_URL}/${url}`, _config)
    return response.data
  }

  async accessExternal(method: HttpMethods, url: string, data?: any): Promise<any> {
    const header = { headers: { ...CONTENT_TYPE } }
    switch (method) {
      case HttpMethods.GET: return axios.get(url)
      case HttpMethods.POST: return axios.post(url, data, header)
    }
  }

  async health(): Promise<boolean> {
    return this.get('health')
  }

  public setHeader(header: object): void {
    _config.headers = { ...CONTENT_TYPE, ...header }
  }
}

const httpService = new HttpService()
export { httpService }
