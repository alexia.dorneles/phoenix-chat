import { Worker, MessageParameters, MessageModel } from '@types'
import { messageFactory } from '@factories'

class MessageWorker implements Worker {
  isApplicable(messageParameters: MessageParameters): boolean {
    return true
  }

  async apply(messageParameters: MessageParameters): Promise<void> {
    const messageModel: MessageModel = {
      dsMensagem: messageParameters.text,
      idLocutor: messageParameters.senderId,
      idInterlocutor: messageParameters.recipientId,
    }

    return messageFactory.saveMessage(messageModel)
  }
}

const messageWorker = new MessageWorker()
export { messageWorker }
