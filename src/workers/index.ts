import { Worker } from '@types'
import { authenticationWorker } from './authenticationWorker'
import { messageWorker } from './messageWorker'

export const workers: Worker[] = [
  authenticationWorker,
  messageWorker,
]
