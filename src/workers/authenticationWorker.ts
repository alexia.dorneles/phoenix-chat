import { Worker, MessageParameters } from '@types'
import { authFactory } from '@factories'
import { facebookBridge } from '@bridges'
import { isPhoenixId } from '@utils'

class AuthenticationWorker implements Worker {
  isApplicable(messageParameters: MessageParameters): boolean {
    return isPhoenixId(messageParameters.recipientId)
  }

  async apply(messageParameters: MessageParameters): Promise<void> {
    await this.login(messageParameters.senderId)
  }

  private async login(userId: number): Promise<void> {
    const userFromFacebook = await facebookBridge.socialSignin(userId)
    await authFactory.login(userFromFacebook)
  }
}

const authenticationWorker = new AuthenticationWorker()
export { authenticationWorker }
