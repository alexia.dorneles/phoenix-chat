import { Usuario } from '@types'
import { httpService, userService } from '@services'

class AuthFactory {
  private currentUserId: number

  async login(usuario: Usuario): Promise<void> {
    const loginResponse = await userService.userRegisterOrLogin(usuario)
    httpService.setHeader({
      authorization: this.getToken(loginResponse.accessToken),
    })

    this.setUserId(loginResponse.idUsuario)
  }

  private getToken(accessToken: string): string {
    return accessToken.replace('Bearer', 'Bearer ')
  }

  private setUserId(userId: number): void {
    this.currentUserId = userId
  }

  public getUserId(): number {
    return this.currentUserId
  }
}

const authFactory = new AuthFactory()
export { authFactory }
