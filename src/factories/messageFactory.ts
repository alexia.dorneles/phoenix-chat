import { messageService } from '@services'
import { MessageModel, MessageResponse } from '@types'
import { isPhoenixId, getPhoenixId } from '@utils'

let LAST_MESSAGE = {} as MessageModel
let ID_MESSAGE_ANSWER: number

class MessageFactory {

  public async saveMessage(message: MessageModel): Promise<void> {
    if (!this.isValidMessage(message)) return

    this.setLastMessage(message)
    await this.handlePhoenixMessage(message)
    await this.handleUserMessage(message)
  }

  private isFromPhoenix(message: MessageModel): boolean {
    return isPhoenixId(message.idLocutor)
  }

  private isFromUser(message: MessageModel): boolean {
    return isPhoenixId(message.idInterlocutor)
  }
  private isValidMessage(message: MessageModel): boolean {
    return !!message.dsMensagem && !message.dsMensagem.includes('undefined')
      && !message.dsMensagem.includes('payload') && !this.isMessageSameAsLast(message)
  }

  private isMessageSameAsLast(message: MessageModel): boolean {
    return message.dsMensagem == LAST_MESSAGE.dsMensagem
      && message.idInterlocutor == LAST_MESSAGE.idInterlocutor
      && message.idLocutor == LAST_MESSAGE.idLocutor
  }

  private setLastMessage(message: MessageModel): void {
    LAST_MESSAGE = message
  }

  private setIdMessageAnswer(idMessageAnswer: number): void {
    ID_MESSAGE_ANSWER = idMessageAnswer
  }

  private async handlePhoenixMessage(message: MessageModel): Promise<void> {
    if (!this.isFromPhoenix(message)) return
    message.idLocutor = getPhoenixId()
    message.idResposta = ID_MESSAGE_ANSWER

    const messageResponse = await this.persist(message)
    this.setIdMessageAnswer(messageResponse.idMensagem)
  }

  private async handleUserMessage(message: MessageModel): Promise<void> {
    if (!this.isFromUser(message)) return

    const messageResponse = await this.persist(message)
    this.setIdMessageAnswer(messageResponse.idMensagem)
  }

  private async persist(model: MessageModel): Promise<MessageResponse> {
    return messageService.save(model)
  }
}

const messageFactory = new MessageFactory()
export { messageFactory }
