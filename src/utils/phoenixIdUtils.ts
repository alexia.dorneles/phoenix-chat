const { PHOENIX_USER_ID } = process.env

export const isPhoenixId = (id: number) => {
  return id == Number(PHOENIX_USER_ID)
}

export const getPhoenixId = () => {
  return Number(PHOENIX_USER_ID)
}
