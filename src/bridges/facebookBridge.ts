import { FacebookProfile, Usuario, HttpMethods } from '@types'
import { httpService } from '@services'

const { FACEBOOK_ACCESS_TOKEN } = process.env

class FacebookBridge {

  private buildUserFromFacebookProfile(profile: FacebookProfile): Usuario {
    return {
      nmUsuario: profile.first_name,
      dsSobrenome: profile.last_name,
      dsLogin: profile.id,
      dsSenha: profile.id,
      dsFoto: profile.profile_pic,
    }
  }

  public async socialSignin(facebookUser: number): Promise<Usuario> {
    const { data } = await httpService.accessExternal(HttpMethods.GET, this.getProfileURL(facebookUser))
    return this.buildUserFromFacebookProfile(data as FacebookProfile)
  }

  private getProfileURL(facebookUser: number): string {
    return `https://graph.facebook.com/${facebookUser}?fields=first_name,last_name,profile_pic,locale&access_token=` + FACEBOOK_ACCESS_TOKEN
  }
}

const facebookBridge = new FacebookBridge()
export { facebookBridge }
