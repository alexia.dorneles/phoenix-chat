export interface Usuario {
  nmUsuario: string
  dsSobrenome: string
  dsLogin: string
  dsSenha: string
  dsFoto: string
}
