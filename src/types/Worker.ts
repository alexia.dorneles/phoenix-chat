import { MessageParameters } from './Message'

export interface Worker {
  isApplicable(messageParameters: MessageParameters): boolean
  apply(messageParameters: MessageParameters): Promise<void>
}
