export interface Event {
  recipient: {
    id: number,
  },
  sender: {
    id: number,
  },
  message: {
    text: string,
  }
}
