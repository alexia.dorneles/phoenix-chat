const URL_BASE = 'http://localhost:50005/phoenix'
const { FACEBOOK_ACCESS_TOKEN } = process.env

export class URLs {
  static get BASE_URL(): string { return URL_BASE }
  static get WEBSOCKET_URL(): string { return `${URL_BASE}/public/ws` }
  static get LINK_QUEUE(): string { return '/fila/vinculo/' }
  static get FACEBOOK_MESSAGE(): string { return `https://graph.facebook.com/v2.6/me/messages?access_token=${FACEBOOK_ACCESS_TOKEN}` }
}
