export interface MessageModel {
  dsMensagem: string,
  idLocutor: number,
  idInterlocutor: number,
  idResposta?: number,
}

export interface MessageResponse {
  idMensagem: number,
  idLocutor: number,
  idInterlocutor: number,
}

export interface MessageParameters {
  text: string,
  senderId: number,
  recipientId: number
}
