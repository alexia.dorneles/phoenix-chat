export interface LoginResponse {
  idUsuario: number
  accessToken: string
}
