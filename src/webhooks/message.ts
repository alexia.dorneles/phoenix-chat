import { Webhook } from '../types/Webhook'
import { processMessage } from './processMessage'

const isPageBody = (req: any) => req.body.object === Webhook.PAGE
const shouldProcessMessage = (event: any) => event.message && event.message.text

export const message = (req: any, res: any) => {
  if (isPageBody(req)) {
    req.body.entry.forEach((entry: any) => {
      entry.messaging.forEach((event: any) => shouldProcessMessage(event) && processMessage(event))
    })

    res.status(200).end()
  }
}
