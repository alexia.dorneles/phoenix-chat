import dialogflow, { WebhookResponse, TextMessage } from 'dialogflow'
import { Event, MessageParameters } from '@types'
import { workers } from '@workers'
import { facebookService } from '@services'

const {
  DIALOGFLOW_PRIVATE_KEY,
  DIALOGFLOW_CLIENT_EMAIL,
  DIALOGFLOW_PROJECT_ID,
  PHOENIX_USER_ID,
} = process.env
const sessionId = '123456'
const languageCode = 'pt-br'

const config = {
  credentials: {
    private_key: DIALOGFLOW_PRIVATE_KEY,
    client_email: DIALOGFLOW_CLIENT_EMAIL
  }
}

const asyncForEach = async (array: Array<any>, asyncFn: (item: any) => Promise<void>): Promise<void> => {
  for (const item of array) {
    await asyncFn(item)
  }
}

const buildMessageParameters = (senderId: number, recipientId: number, text: string): MessageParameters => {
  return { senderId, recipientId, text }
}

const sessionClient = new dialogflow.SessionsClient(config)
const sessionPath = sessionClient.sessionPath(DIALOGFLOW_PROJECT_ID, sessionId)

// SEND MESSAGE
const sendTextMessage = async (messageParameters: MessageParameters, response: any) => {
  if (response.queryResult && response.queryResult.queryText) {
    const webhookValidResponse = response.queryResult as WebhookResponse
    for (const fulfillmentMessage of webhookValidResponse.fulfillmentMessages) {
      const fulfillmentMessageCasted = fulfillmentMessage as TextMessage
      const text = fulfillmentMessageCasted.text.text[0]
      if (text) {
        await facebookService.sendMessageBubble(messageParameters.senderId)
        await facebookService.sendMessage(text, messageParameters.senderId)
      }
    }
  }
}

// RECEIVE MESSAGE
export const processMessage = async (event: Event): Promise<void> => {
  const senderId = event.sender.id
  const recipientId = event.recipient.id
  const message = event.message.text

  const messageParameters = buildMessageParameters(senderId, recipientId, message)
  const applicableWorkers = workers.filter(worker => worker.isApplicable(messageParameters))
  await asyncForEach(applicableWorkers, worker => worker.apply(messageParameters))

  if (_isPhoenixMessage(senderId)) {
    return
  }

  const request = {
    session: sessionPath,
    queryInput: {
      text: { text: message, languageCode },
    },
  }


  try {
    const responses = await sessionClient.detectIntent(request)
    await sendTextMessage(messageParameters, responses[0])
  } catch (err) {
    console.error('ERROR on dialogflow:', err)
  }
}

const _isPhoenixMessage = (senderId: number): boolean => {
  return senderId == Number(PHOENIX_USER_ID)
}
