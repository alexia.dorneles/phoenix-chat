const { VERIFY_TOKEN } = process.env

const successResponse = (res: any, challenge: any) => res.status(200).send(challenge)
const errorResponse = (res: any) => res.sendStatus(403)

export const verifier = (req: any, res: any): void => {
  const mode = req.query['hub.mode']
  const token = req.query['hub.verify_token']
  const challenge = req.query['hub.challenge']

  return mode && token === VERIFY_TOKEN
    ? successResponse(res, challenge)
    : errorResponse(res)
}

